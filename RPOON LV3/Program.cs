﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            //PRVI
            Dataset firstDataSet = new Dataset("file.txt");
            Console.WriteLine("firstDataSet:\n"+firstDataSet.ToString());
            Dataset secondDataSet = (Dataset)firstDataSet.Clone();
            Console.WriteLine("secondDataSet:\n" + secondDataSet.ToString());
            firstDataSet.MakeChange();
            Console.WriteLine("firstDataSet:\n" + firstDataSet.ToString());
            Console.WriteLine("secondDataSet:\n" + secondDataSet.ToString());

            //DRUGI
            MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
            double [,]myArray = matrixGenerator.GenerateMatrix(5,5);
            for(int i=0;i<5;i++)
            {
                for(int j=0;j<5;j++)
                {
                    Console.Write(myArray[i, j]+" ");
                }
                Console.WriteLine("\n");
            }

            //TRECI
            Logger logger = Logger.GetInstance();
            logger.Log("My new text, yay.");

            //CETVRTI
            NotificationManager notificationManager = new NotificationManager();
            ConsoleNotification consoleNotification = new ConsoleNotification("Sale", "Obavijest", "Random obavijest", DateTime.Now, Category.INFO, ConsoleColor.Green);
            notificationManager.Display(consoleNotification);

            //PETI
            IBuilder notificationBuilder = new NotificationBuilder();
            notificationManager.Display(notificationBuilder.SetAuthor("Sale").SetTitle("Obavijest").SetText("Random obavijest").SetTime(DateTime.Now).SetLevel(Category.ALERT).SetColor(ConsoleColor.Yellow).Build());

            //SESTI
            NotificationDirector notificationDirector = new NotificationDirector(notificationBuilder);
            ConsoleNotification alert = notificationDirector.CreateAlertNotification("Viduka");
            ConsoleNotification info = notificationDirector.CreateInfoNotification("Šuc");
            ConsoleNotification error = notificationDirector.CreateErrorNotification("Dražen");
            notificationManager.Display(error);
            notificationManager.Display(alert);
            notificationManager.Display(info);

            //SEDMI
            ConsoleNotification secondConsoleNotification = (ConsoleNotification)consoleNotification.Clone();
            
            
        }
    }
}
