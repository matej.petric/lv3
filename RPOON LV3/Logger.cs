﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV3
{
    class Logger
    {
        private static Logger instance;
        private string filepath { get; set; }

        private Logger()
        {
            filepath = "unnamed.txt";
        }

        public static Logger GetInstance()
        {
            if(instance==null)
            {
                instance = new Logger();
            }
            return instance;
        }


        public void Log(string text)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(filepath))
            {
                writer.Write(text);
            }
        }
    }
}
