﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV3
{
    class Dataset:Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }

        public void MakeChange()
        {
            data[0][0] = "da";
        }


        public void SetData(IList<List<string>> Data)
        {
            data.Clear();
            data = Data.ToList();
        }
        public Prototype Clone()
        {
            Dataset newDataset = new Dataset();

            foreach(List<string> row in data)
            {
                List<string> newRow = new List<string>(row);
                //foreach(string word in row)
                //{
                //    newRow.Add(word);
                //}
                newDataset.data.Add(newRow);
            }
            
            return newDataset;
        }
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach(List<string> row in data)
            {
                foreach(string word in row)
                {
                    stringBuilder.Append(word);
                }
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }

    }
}
