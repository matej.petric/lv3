﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV3
{
    class NotificationBuilder:IBuilder
    {
        private string author;
        private string title;
        private DateTime datetime;
        private Category level;
        private ConsoleColor color;
        private string text;

        public NotificationBuilder()
        {
            author = string.Empty; title = string.Empty; datetime = DateTime.Now; level = Category.INFO; text = string.Empty;
        }
        public ConsoleNotification Build()
        {
            return new ConsoleNotification(author, title, text, datetime, level, color);
        }

        public IBuilder SetAuthor(string Author)
        {
            author = Author;
            return this;
        }
        public IBuilder SetText(string Text)
        {
            text = Text;
            return this;
        }
        public IBuilder SetTime(DateTime Time)
        {
            datetime = Time;
            return this;
        }
        public IBuilder SetLevel(Category Level)
        {
            level = Level;
            return this;
        }
        public IBuilder SetColor(ConsoleColor Color)
        {
            color = Color;
            return this;
        }
        public IBuilder SetTitle(string Title)
        {
            title = Title;
            return this;
        }
    }
}
