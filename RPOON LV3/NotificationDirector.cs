﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV3
{
    class NotificationDirector
    {
        private IBuilder builder;

        public NotificationDirector(IBuilder Builder)
        {
            builder = Builder;
        }

        public ConsoleNotification CreateInfoNotification(string Author)
        {
            return builder.SetAuthor(Author).SetColor(ConsoleColor.Blue).SetTime(DateTime.Now).SetTitle("Info").SetText("New info.").Build();
        }

        public ConsoleNotification CreateAlertNotification(string Author)
        {
            return builder.SetAuthor(Author).SetColor(ConsoleColor.DarkRed).SetTime(DateTime.Now).SetTitle("Alert").SetText("Alarming state!").Build();
        }

        public ConsoleNotification CreateErrorNotification(string Author)
        {
            return builder.SetAuthor(Author).SetColor(ConsoleColor.Red).SetTime(DateTime.Now).SetTitle("ERROR").SetText("Error occured.").Build();
        }
    }
}
